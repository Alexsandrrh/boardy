import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import BoardPage from './pages/BoardPage';
import BoardsPage from './pages/BoardsPage';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={BoardsPage} />
          <Route path='/b/:id' component={BoardPage} />
          <Route path='/board/:id' component={BoardPage} />
        </Switch>
      </Router>
    );
  }
}

export default App;
