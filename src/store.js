import { combineReducers } from 'redux';
import { board } from './reducer/board';
import { boards } from './reducer/boards';
import { histories } from './reducer/histories';

export default combineReducers({
  board,
  boards,
  histories
});
