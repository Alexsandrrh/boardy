import React, { Component } from 'react';
import './History.scss';
import { Link } from 'react-router-dom';
import moment from 'moment';

class History extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { message, createAt, board } = this.props.history;

    return (
      <Link to={'/b/' + board.id} className='history-item'>
        <div className='history-item__head'>
          <span className='history-item__emoji'>{board.emoji}</span>
          <div className='history-item__content'>
            <h3 className='history-item__title'>{board.title}</h3>
            <span className='history-item__time'>
              {moment(createAt)
                .locale('ru')
                .fromNow()}
            </span>
          </div>
        </div>
        <div className='history-item__body'>
          <p className='history-item__message'>{message}</p>
        </div>
      </Link>
    );
  }
}

export default History;
