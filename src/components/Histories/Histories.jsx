import React, { Component } from 'react';
import History from './History/History';
import './Histories.scss';

class Histories extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const HistoriesList = this.props.histories.map(item => {
      return <History key={item.id} history={item} />;
    });

    return (
      <div className='histories-list'>
        <div className='histories-list__head'>
          <h2 className='histories-list__title'>Ваша активность</h2>
        </div>
        <div className='histories-list__body'>{HistoriesList}</div>
      </div>
    );
  }
}

export default Histories;
