import React from 'react';

const Icon = props => (
  <svg className={props.otherClass + ' -icon'}>
    <use xlinkHref={props.icon} />
  </svg>
);

export default Icon;
