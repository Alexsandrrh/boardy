import React, { Component } from 'react';
import './Modal.scss';

class Modal extends Component {
  render() {
    const { isOpen } = this.props;

    if (isOpen === false) {
      return null;
    } else {
      return (
        <div className={'modal -' + this.props.name}>
          <div
            className='modal__bg'
            onClick={e => {
              this.closeModalWindow(e);
            }}
          />
          <div className='modal__body'>{this.props.children}</div>
        </div>
      );
    }
  }

  closeModalWindow(e) {
    e.preventDefault();

    if (this.props.onClose) {
      this.props.onClose();
    }
  }
}

export default Modal;
