import React, { Component } from 'react';
import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';
import './AddBlock.scss';
import Textarea from 'react-textarea-autosize';
import IconAdd from '../../../assets/icons/add.svg';
import IconExit from '../../../assets/icons/cross.svg';
import Icon from '../Icon/Icon';

class AddBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isShow: false
    };

    this.onClickOpen = this.onClickOpen.bind(this);
    this.onClickClose = this.onClickClose.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onClickOpen() {
    this.refs.dropdown.show();
  }

  onClickClose() {
    this.refs.dropdown.hide();
  }

  render() {
    const { title, submitBtn, placeholder, type, round } = this.props;

    const chooseTypeForm = () => {
      switch (type) {
        case 'input':
          return (
            <input
              className='add-block__input'
              ref={e => (this.formText = e)}
              placeholder={placeholder}
            />
          );
          break;
        case 'textarea':
          return (
            <Textarea
              className='add-block__textarea'
              onChange={e => (this.formText = e.target)}
              placeholder={placeholder}
            />
          );
          break;
      }
    };

    return (
      <Dropdown
        onShow={() => {
          this.setState({ isShow: true });
          this.props.onOpen();
        }}
        onHide={() => {
          this.setState({ isShow: false });
          this.props.onClose();
        }}
        className={'add-block -' + round}
        ref='dropdown'
      >
        <DropdownTrigger>
          <button className='add-block__button -open box-container' onClick={this.onClickOpen}>
            <Icon icon={IconAdd} otherClass='add-block__icon' />
            <span className='add-block__title'>{title}</span>
          </button>
        </DropdownTrigger>
        <DropdownContent>
          {this.state.isShow ? (
            <form
              className='add-block__form box-container'
              onSubmit={e => {
                this.onSubmit(e);
              }}
            >
              {chooseTypeForm()}
              <div className='add-block__action'>
                <button
                  className='add-block__button -add'
                  onClick={e => {
                    this.onSubmit(e);
                  }}
                >
                  <span className='add-block__title'>{submitBtn}</span>
                </button>
                <button className='add-block__button -close' onClick={this.onClickClose}>
                  <Icon icon={IconExit} otherClass='add-block__icon' />
                </button>
              </div>
            </form>
          ) : null}
        </DropdownContent>
      </Dropdown>
    );
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.formText.value);

    this.refs.dropdown.hide();
  }
}

export default AddBlock;
