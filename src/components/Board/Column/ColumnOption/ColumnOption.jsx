import React, { Component } from 'react';
import Icon from '../../../Common/Icon/Icon';
import IconOption from '../../../../assets/icons/ellipsis-h.svg';
import IconTrash from '../../../../assets/icons/trash.svg';
import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';

class ColumnOption extends Component {
  constructor(props) {
    super(props);

    this.onClickOpen = this.onClickOpen.bind(this);
    this.onClickClose = this.onClickClose.bind(this);
  }

  onClickOpen() {
    this.refs.dropdown.show();
  }

  onClickClose() {
    this.refs.dropdown.hide();
  }

  render() {
    return (
      <Dropdown ref='dropdown'>
        <DropdownTrigger>
          <button className='column-item__button' onClick={this.onClickOpen}>
            <Icon icon={IconOption} otherClass='column-item__icon' />
          </button>
        </DropdownTrigger>
        <DropdownContent>
          <button
            className='column-item__button -trash'
            onClick={e => {
              this.onTrash(e);
              this.onClickClose();
            }}
          >
            <Icon icon={IconTrash} otherClass='column-item__icon' />
          </button>
        </DropdownContent>
      </Dropdown>
    );
  }

  onTrash(e) {
    e.preventDefault();

    if (this.props.onTrashColumn) {
      this.props.onTrashColumn();
    }
  }
}

export default ColumnOption;
