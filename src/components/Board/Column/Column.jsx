import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import './Column.scss';
import Card from './Card/Card';
import AddBlock from '../../Common/AddBlock/AddBlock';
import { postNewCard } from '../../../action/cardAction';
import { putTitleColumn, deleteColumn } from '../../../action/columnAction';
import { connect } from 'react-redux';
import ColumnOption from './ColumnOption/ColumnOption';

class Column extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpenForm: false
    };

    this.onCloseForm = this.onCloseForm.bind(this);
    this.onOpenForm = this.onOpenForm.bind(this);
  }

  render() {
    const { id, title, idBoard } = this.props.column;

    const addClassCard = this.state.isOpenForm ? ' -open-form' : '';

    return (
      <div className='column-wrapper'>
        <div className='column-item'>
          <div className='column-item__head'>
            <input
              className='column-item__title'
              type='text'
              placeholder='Название колонки'
              defaultValue={title}
              onChange={e => {
                this.props.putTitleColumn(idBoard, id, e.target.value);
              }}
            />
            <ColumnOption
              onTrashColumn={() => {
                this.props.deleteColumn(idBoard, id);
              }}
            />
          </div>

          <Droppable droppableId={id}>
            {provided => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className='column-item__body'
              >
                <div
                  className={'column-item__cards' + addClassCard}
                  ref={el => (this.blockCards = el)}
                >
                  {this.props.cards.map((item, index) => {
                    return <Card key={item.id} index={index} data={item} />;
                  })}

                  {provided.placeholder}
                </div>
                <div className='column-item__button'>
                  <AddBlock
                    title='Добавить еще одну карточку'
                    submitBtn='Добавить карточку'
                    round='bottom'
                    type='textarea'
                    placeholder='Введите название карточки'
                    onOpen={this.onOpenForm}
                    onClose={this.onCloseForm}
                    onSubmit={title => {
                      this.props.postNewCard(idBoard, id, title);
                    }}
                  />
                </div>
              </div>
            )}
          </Droppable>
        </div>
      </div>
    );
  }

  onOpenForm() {
    this.setState({ isOpenForm: true });
    this.blockCards.scrollTop = this.blockCards.scrollHeight;
  }

  onCloseForm() {
    this.setState({ isOpenForm: false });
    this.blockCards.scrollTop = this.blockCards.scrollHeight;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    postNewCard: (idBoard, idColumn, title) => {
      dispatch(postNewCard(idBoard, idColumn, title));
    },
    putTitleColumn: (idBoard, idColumn, title) => {
      dispatch(putTitleColumn(idBoard, idColumn, title));
    },
    deleteColumn: (idBoard, idColumn) => {
      dispatch(deleteColumn(idBoard, idColumn));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Column);
