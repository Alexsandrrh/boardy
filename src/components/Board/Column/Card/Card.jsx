import React, { Component } from 'react';
import './Card.scss';
import { Draggable } from 'react-beautiful-dnd';

class Card extends Component {
  render() {
    const { id, title } = this.props.data;

    return (
      <Draggable draggableId={id} index={this.props.index}>
        {provided => (
          <div
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            className='card-item'
          >
            <div className='card-item__container box-container'>
              <p className='card-item__title'>{title}</p>
            </div>
          </div>
        )}
      </Draggable>
    );
  }
}

export default Card;
