import React, { Component } from 'react';
import './Board.scss';
import Column from './Column/Column';
import AddBlock from '../Common/AddBlock/AddBlock';
import { DragDropContext } from 'react-beautiful-dnd';
import { postNewColumn } from '../../action/columnAction';
import { changePlaceCard } from '../../action/cardAction';
import { connect } from 'react-redux';

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onDragEnd = this.onDragEnd.bind(this);
  }

  render() {
    const { id, columnsOrder, columns, cards } = this.props.board;

    return (
      <div className='board-view'>
        <DragDropContext onDragEnd={this.onDragEnd}>
          {columnsOrder.map(column => {
            const objectColumn = columns[column];
            const arrayCards = objectColumn.cards.map(card => cards[String(card)]);

            return <Column key={objectColumn.id} column={objectColumn} cards={arrayCards} />;
          })}
          <div className='column-wrapper'>
            <AddBlock
              title='Добавить еще одну колонку'
              submitBtn='Добавить колонку'
              round='all'
              type='input'
              onOpen={() => {}}
              onClose={() => {}}
              placeholder='Введите название колонки'
              onSubmit={title => {
                this.props.postNewColumn(id, title);
              }}
            />
          </div>
        </DragDropContext>
      </div>
    );
  }

  onDragEnd(result) {
    const { destination, source } = result;

    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    this.props.changePlaceCard(this.props.board.id, source, destination);
  }
}

const mapDispatchToProps = dispatch => {
  return {
    postNewColumn: (idBoard, title) => {
      dispatch(postNewColumn(idBoard, title));
    },
    changePlaceCard: (idBoard, idCard, idColumn, index) => {
      dispatch(changePlaceCard(idBoard, idCard, idColumn, index));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Board);
