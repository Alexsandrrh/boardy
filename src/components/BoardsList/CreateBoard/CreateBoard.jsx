import React, { Component, Fragment } from 'react';
import './CreateBoard.scss';
import { createPortal } from 'react-dom';
import Icon from '../../Common/Icon/Icon';
import IconAdd from '../../../assets/icons/add.svg';
import IconClose from '../../../assets/icons/multiply.svg';
import ImageSuccess from '../../../assets/images/appreciation.gif';
import Modal from '../../Common/Modal/Modal';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { postNewBoard } from '../../../action/boardsAction';
import { getDataHistories } from '../../../action/historiesAction';

class CreateBoard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  render() {
    return (
      <Fragment>
        <button className='board-card -new' onClick={this.toggleModal}>
          <Icon icon={IconAdd} otherClass='board-card__icon' />
        </button>

        {createPortal(
          <Modal name='new-board' isOpen={this.state.isModalOpen} onClose={this.toggleModal}>
            <div className='create-board'>
              <button className='create-board__button' onClick={this.toggleModal}>
                <Icon icon={IconClose} otherClass='create-board__icon' />
              </button>
              <div className='create-board__preloader'>
                <img src={ImageSuccess} />
              </div>
              <form
                className='create-board__form'
                onSubmit={e => {
                  this.onSubmit(e);
                }}
              >
                <input
                  type='text'
                  autoFocus={true}
                  placeholder='Напиши название'
                  ref={e => (this.titleBoard = e)}
                  className='create-board__input'
                />
                <p className='create-board__desc'>
                  Чтобы создать новую магическую доску нажмите Enter. Emoji для доски генерироется
                  автоматически. В дальнейшем вы сможете их менять с помощью магии.
                </p>
              </form>
            </div>
          </Modal>,
          document.getElementById('modal')
        )}
      </Fragment>
    );
  }

  toggleModal() {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.postNewBoard(this.titleBoard.value);
    this.props.getDataHistories();
    this.toggleModal();
  }
}

const mapDispatchToProps = dispatch => {
  return {
    postNewBoard: title => {
      dispatch(postNewBoard(title));
    },
    getDataHistories: () => {
      dispatch(getDataHistories());
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withRouter(CreateBoard));
