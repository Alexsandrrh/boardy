import React, { Component } from 'react';
import './BoardCard.scss';
import { Link } from 'react-router-dom';

class BoardCard extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { title, emoji, id } = this.props.board;

    return (
      <Link to={'/b/' + id} className='board-card'>
        <span className='board-card__emoji'>{emoji}</span>
        <p className='board-card__title'>{title}</p>
      </Link>
    );
  }
}

export default BoardCard;
