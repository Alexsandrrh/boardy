import React, { Component } from 'react';
import './BoardsList.scss';
import BoardCard from './BoardCard/BoardCard';
import CreateBoard from './CreateBoard/CreateBoard';

class BoardsList extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const Boards = this.props.boards.map(board => {
      return <BoardCard key={board.id} board={board} />;
    });

    return (
      <div className='boards-list'>
        {Boards}
        <CreateBoard />
      </div>
    );
  }
}

export default BoardsList;
