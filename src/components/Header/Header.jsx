import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';
import Icon from '../Common/Icon/Icon';
import IconMenu from '../../assets/icons/menu.svg';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpenMenu: false
    };
  }

  render() {
    const { title, emoji, id } = this.props.board;

    return (
      <header className='header'>
        <div className='container'>
          <Link to='/' className='header-container__button header-hover'>
            <Icon icon={IconMenu} otherClass='header-container__icon' />
          </Link>
          <div className='header-container__title header-hover'>
            <span className='header-container__emoji' onClick={e => this.changeEmoji(e)}>
              {emoji}
            </span>
            <input
              className='header-container__input'
              type='text'
              placeholder='Название доски'
              defaultValue={title}
              onChange={e => {
                this.props.onChangeTitle(id, e.target.value);
              }}
            />
          </div>
        </div>
      </header>
    );
  }

  changeEmoji(e) {
    e.preventDefault();

    if (this.props.onChangeEmoji) {
      this.props.onChangeEmoji(this.props.board.id);
    }
  }
}

export default Header;
