import { GET_DATA_BOARDS_SUCCESS, POST_NEW_BOARD_SUCCESS } from '../constants';

export function boards(state = [], action) {
  switch (action.type) {
    case GET_DATA_BOARDS_SUCCESS:
      return action.boards;
      break;
    case POST_NEW_BOARD_SUCCESS:
      return [...state, action.board];
      break;
    default:
      return state;
  }
}
