import {
  GET_DATA_BOARD_SUCCESS,
  POST_NEW_COLUMN_SUCCESS,
  POST_NEW_CARD_SUCCESS,
  PUT_TITLE_COLUMN_SUCCESS,
  PUT_TITLE_BOARD_SUCCESS,
  PUT_EMOJI_BOARD_SUCCESS,
  DELETE_COLUMN_SUCCESS,
  CHANGE_PLACE_CARD_SUCCESS
} from '../constants';

const staticBoard = {
  cards: {},
  columns: {},
  columnsOrder: []
};

export function board(state = staticBoard, action) {
  switch (action.type) {
    case GET_DATA_BOARD_SUCCESS:
      return { ...action.board };
      break;
    case PUT_EMOJI_BOARD_SUCCESS:
      return {
        ...state,
        emoji: action.emoji.code
      };
      break;
    case PUT_TITLE_BOARD_SUCCESS:
      return {
        ...state,
        title: action.title
      };
      break;
    case POST_NEW_COLUMN_SUCCESS:
      return {
        ...state,
        columns: action.data.columns,
        columnsOrder: action.data.columnsOrder
      };
      break;
    case PUT_TITLE_COLUMN_SUCCESS:
      return {
        ...state,
        columns: action.columns
      };
      break;
    case DELETE_COLUMN_SUCCESS:
      return {
        ...state,
        columns: action.data.columns,
        columnsOrder: action.data.columnsOrder
      };
      break;
    case POST_NEW_CARD_SUCCESS:
      return {
        ...state,
        cards: action.data.cards,
        columns: action.data.columns
      };
      break;
    case CHANGE_PLACE_CARD_SUCCESS:
      const { source, destination } = action.data;

      const BoardValue = state;
      let Columns = BoardValue.columns;

      // Find column with CardMoved
      let lastColumn = Columns[String(source.droppableId)];
      // Find CardMoved and delete her from last column
      let cardMoved = lastColumn.cards.splice(source.index, 1)[0];
      // Add CardMoved to new column
      let newColumn = Columns[String(destination.droppableId)];
      newColumn.cards.splice(destination.index, 0, cardMoved);

      return {
        ...state,
        columns: Columns
      };
      break;

    default:
      return staticBoard;
  }
}
