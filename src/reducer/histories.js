import { GET_DATA_HISTORIES } from '../constants';

export function histories(state = [], action) {
  switch (action.type) {
    case GET_DATA_HISTORIES:
      return action.histories;
    default:
      return state;
  }
}
