import {
  POST_NEW_COLUMN_SUCCESS,
  PUT_TITLE_COLUMN_SUCCESS,
  DELETE_COLUMN_SUCCESS
} from '../constants';
import axios from 'axios';

export function postNewColumnSuccess(data) {
  return {
    type: POST_NEW_COLUMN_SUCCESS,
    data
  };
}

export function postNewColumn(idBoard, title) {
  return dispatch => {
    axios
      .post(`/api/column`, {
        title,
        idBoard
      })
      .then(res => {
        dispatch(postNewColumnSuccess(res.data));
      });
  };
}

export function putTitleColumnSuccess(columns) {
  return {
    type: PUT_TITLE_COLUMN_SUCCESS,
    columns
  };
}

export function putTitleColumn(idBoard, idColumn, title) {
  return dispatch => {
    axios
      .put(`/api/column/title`, {
        title,
        idBoard,
        idColumn
      })
      .then(res => {
        dispatch(putTitleColumnSuccess(res.data));
      });
  };
}

export function deleteColumnSuccess(data) {
  return {
    type: DELETE_COLUMN_SUCCESS,
    data
  };
}

export function deleteColumn(idBoard, idColumn) {
  return dispatch => {
    axios
      .delete(`/api/column`, {
        data: {
          idBoard,
          idColumn
        }
      })
      .then(res => {
        dispatch(deleteColumnSuccess(res.data));
      });
  };
}
