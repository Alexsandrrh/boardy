import { GET_DATA_BOARDS_SUCCESS, POST_NEW_BOARD_SUCCESS } from '../constants';
import axios from 'axios';

export function getDataBoardsSuccess(boards) {
  return {
    type: GET_DATA_BOARDS_SUCCESS,
    boards
  };
}

export function getDataBoards() {
  return dispatch => {
    axios.get('/api/boards').then(res => {
      dispatch(getDataBoardsSuccess(res.data));
    });
  };
}

export function postNewBoardSuccess(board) {
  return {
    type: POST_NEW_BOARD_SUCCESS,
    board
  };
}

export function postNewBoard(title) {
  return dispatch => {
    axios.post('/api/board', { title }).then(res => {
      dispatch(postNewBoardSuccess(res.data));
    });
  };
}
