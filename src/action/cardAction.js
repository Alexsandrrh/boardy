import {
  POST_NEW_CARD_SUCCESS,
  CHANGE_PLACE_CARD_SUCCESS,
  CHANGE_PLACE_CARD_FAILED
} from '../constants';
import axios from 'axios';

export function postNewCardSuccess(data) {
  return {
    type: POST_NEW_CARD_SUCCESS,
    data
  };
}

export function postNewCard(idBoard, idColumn, title) {
  return dispatch => {
    axios
      .post(`/api/card`, {
        title,
        idBoard,
        idColumn
      })
      .then(res => {
        dispatch(postNewCardSuccess(res.data));
      });
  };
}

export function changePlaceCardSuccess(data) {
  return {
    type: CHANGE_PLACE_CARD_SUCCESS,
    data
  };
}

export function changePlaceCard(idBoard, source, destination) {
  return dispatch => {
    dispatch(changePlaceCardSuccess({ source, destination }));

    axios
      .post('/api/card/change', {
        idBoard,
        source,
        destination
      })
      .catch(err => {
        return err;
      });
  };
}
