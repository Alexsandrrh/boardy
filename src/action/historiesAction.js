import { GET_DATA_HISTORIES } from '../constants';
import axios from 'axios';

export function getDataHistoriesSuccess(histories) {
  return {
    type: GET_DATA_HISTORIES,
    histories
  };
}

export function getDataHistories() {
  return dispatch => {
    axios.get(`/api/histories`).then(res => {
      dispatch(getDataHistoriesSuccess(res.data));
    });
  };
}
