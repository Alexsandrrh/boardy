import {
  GET_DATA_BOARD_SUCCESS,
  PUT_EMOJI_BOARD_SUCCESS,
  PUT_TITLE_BOARD_SUCCESS
} from '../constants';
import axios from 'axios';

export function getDataBoardSuccess(board) {
  return {
    type: GET_DATA_BOARD_SUCCESS,
    board
  };
}

export function getDataBoard(id) {
  return dispatch => {
    axios.get(`/api/board/${id}`).then(res => {
      dispatch(getDataBoardSuccess(res.data));
    });
  };
}

export function changeEmojiBoardSuccess(emoji) {
  return {
    type: PUT_EMOJI_BOARD_SUCCESS,
    emoji
  };
}

export function changeEmojiBoard(id) {
  return dispatch => {
    axios
      .put(`/api/board/emoji`, {
        id
      })
      .then(res => {
        dispatch(changeEmojiBoardSuccess(res.data));
      });
  };
}

export function changeTitleBoardSuccess(title) {
  return {
    type: PUT_TITLE_BOARD_SUCCESS,
    title
  };
}

export function changeTitleBoard(id, title) {
  return dispatch => {
    axios
      .put(`/api/board/title`, {
        id,
        title
      })
      .then(res => {
        dispatch(changeTitleBoardSuccess(res.data));
      });
  };
}
