import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Board from '../components/Board/Board';
import Header from '../components/Header/Header';
import { getDataBoard, changeEmojiBoard, changeTitleBoard } from '../action/boardAction';

class BoardPage extends Component {
  componentDidMount() {
    this.props.getDataBoard(this.props.match.params.id);
  }

  render() {
    const board = this.props.board;

    return (
      <Fragment>
        <Header
          board={board}
          onChangeEmoji={id => {
            this.props.changeEmojiBoard(id);
          }}
          onChangeTitle={(id, title) => {
            this.props.changeTitleBoard(id, title);
          }}
        />
        <div className='board-body'>
          <Board board={board} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    board: state.board
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDataBoard: id => {
      dispatch(getDataBoard(id));
    },
    changeEmojiBoard: id => {
      dispatch(changeEmojiBoard(id));
    },
    changeTitleBoard: (id, title) => {
      dispatch(changeTitleBoard(id, title));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoardPage);
