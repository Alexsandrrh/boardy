import React, { Component } from 'react';
import { getDataBoards } from '../action/boardsAction';
import { getDataHistories } from '../action/historiesAction';
import { connect } from 'react-redux';
import BoardsList from '../components/BoardsList/BoardsList';
import Histories from '../components/Histories/Histories';

class BoardsPage extends Component {
  componentDidMount() {
    this.props.getDataBoards();
    this.props.getDataHistories();
    document.title = 'Мои доски';
  }

  render() {
    return (
      <div className='boards-page'>
        <div className='boards-page__head'>
          <h2 className='boards-page__headline'>
            Мои доски <span className='boards-page__counter'>{this.props.boards.length}</span>
          </h2>
        </div>
        <div className='boards-page__body'>
          <BoardsList boards={this.props.boards} />
        </div>
        <Histories histories={this.props.histories} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    boards: state.boards,
    histories: state.histories
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDataBoards: () => {
      dispatch(getDataBoards());
    },
    getDataHistories: () => {
      dispatch(getDataHistories());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoardsPage);
