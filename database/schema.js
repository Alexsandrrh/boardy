module.exports = function schema(object) {
  object.id = String(require('bson-objectid/objectid')());
  object.createAt = Date.now();
  return object;
};
