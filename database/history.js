const Schema = require('./schema');
module.exports.history = (board, message) => {
  return Schema({
    board,
    message
  });
};
