const Schema = require('./schema');

const staticCard = (Board, idColumn, title) => {
  return Schema({
    idBoard: Board.id,
    idColumn,
    title
  });
};

const staticColumn = (idBoard, title, index) => {
  return Schema({
    title,
    idBoard,
    cards: []
  });
};

const staticBoard = Schema({
  title: 'Добро пожаловать!',
  emoji: '🌈',
  cards: {},
  columns: {},
  columnsOrder: [],
  background: null
});

let firstColumn = staticColumn(staticBoard.id, 'Что тут можно делать?');
let secondColumn = staticColumn(staticBoard.id, 'Попробуте перетащить');

const arrayCards = [
  '1. Вы можете создавать доски',
  '2. Создавать колонки и дополнять их разными карточками с разными заданиями, так же вы можете удалять не нужные колонки',
  '3. Доску можно кастомизировать под себя, менять название, emoji',
  'Автор проекта: Александр Садов http://alexsandrrh.surge.sh/'
];

arrayCards.forEach(text => {
  const newCard = staticCard(staticBoard, firstColumn.id, text);
  staticBoard.cards[newCard.id] = newCard;
  firstColumn.cards.push(newCard.id);
});

staticBoard.columns[firstColumn.id] = firstColumn;
staticBoard.columns[secondColumn.id] = secondColumn;

staticBoard.columnsOrder.push(firstColumn.id);
staticBoard.columnsOrder.push(secondColumn.id);

module.exports = staticBoard;
