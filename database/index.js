const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const DB = low(adapter);

// Set some defaults
DB.defaults({
  boards: [require('./startkit')],
  histories: []
}).write();

module.exports = {
  DB,
  Schema: require('./schema')
};
