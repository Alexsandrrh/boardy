const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const path = require('path');

const { argv } = require('yargs');
const isMiddleware = argv.middleware;

app.use(async (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  next();
});

app.use(bodyParser.json());

if (isMiddleware) {
  const webpack = require('webpack');
  const webpackConfig = require('./webpack.config');
  const compiler = webpack(webpackConfig);
  app.use(
    require('webpack-dev-middleware')(compiler, {
      noInfo: true,
      publicPath: '/'
    })
  );
  app.use(require('webpack-hot-middleware')(compiler));
} else {
  app.use(express.static('dist'));
}

app.use('/api', require('./router/board'));
app.use('/api', require('./router/column'));
app.use('/api', require('./router/card'));
app.use('/api', require('./router/history'));

app.all('*', function(req, res) {
  res.sendFile(path.resolve('dist', 'index.html'));
});

app.listen(80, () => {
  // eslint-disable-next-line
  console.log('Server was started...\nhttp://localhost:80');
});
