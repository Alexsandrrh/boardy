const router = require('express').Router();
const { DB, Schema } = require('../database');
const { history } = require('../database/history');

router.get('/boards', (req, res) => {
  res.json(DB.get('boards').value());
});

router.get('/board/:id', (req, res) => {
  res.json(
    DB.get('boards')
      .find({ id: req.params.id })
      .value()
  );
});

router.post('/board', (req, res) => {
  const { title } = req.body;
  const BOARD = Schema({
    emoji: require('node-emoji').random().emoji,
    title: title,
    columns: {},
    cards: {},
    columnsOrder: []
  });

  DB.get('boards')
    .push(BOARD)
    .write();

  DB.get('histories')
    .push(
      history(
        { emoji: BOARD.emoji, title: BOARD.title, id: BOARD.id },
        `Создана новая доска с названием "${BOARD.title}"`
      )
    )
    .write();
  res.json(BOARD);
});

router.put('/board/emoji', (req, res) => {
  const { id } = req.body;
  const Board = DB.get('boards').find({ id: id });
  const BoardValue = Board.value();
  const newEmoji = require('node-emoji').random();

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Изменилась Emoji у доски с названием "${BoardValue.title}" c ${BoardValue.emoji} на ${
          newEmoji.emoji
        }`
      )
    )
    .write();

  Board.assign({ emoji: newEmoji.emoji }).write();

  res.json({ code: newEmoji.emoji });
});

router.put('/board/title', (req, res) => {
  const { id, title } = req.body;
  const Board = DB.get('boards').find({ id: id });
  const BoardValue = Board.value();

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Изменилось название у доски "${BoardValue.title}" на "${title}"`
      )
    )
    .write();

  Board.assign({ title: title }).write();

  res.json({ text: title });
});

module.exports = router;
