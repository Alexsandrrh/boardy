const router = require('express').Router();
const { DB } = require('../database');
const arraySort = require('array-sort');

router.get('/histories', (req, res) => {
  res.json(arraySort(DB.get('histories').value(), 'createAt').reverse());
});

module.exports = router;
