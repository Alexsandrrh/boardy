const router = require('express').Router();
const { DB, Schema } = require('../database');
const { history } = require('../database/history');

router.post('/column', (req, res) => {
  const { idBoard, title } = req.body;
  const Board = DB.get('boards').find({ id: idBoard });
  const BoardValue = Board.value();
  let Columns = BoardValue.columns;
  let ColumnsOrder = BoardValue.columnsOrder;

  // Create new Scheme Column
  const newColumn = Schema({
    title,
    idBoard,
    cards: []
  });

  // Add new column in columns
  Columns[String(newColumn.id)] = newColumn;

  // Push new column in columnOrder
  ColumnsOrder.push(String(newColumn.id));

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Создана новая колонка "${BoardValue.title}" в доске ${BoardValue.emoji}${BoardValue.title}`
      )
    )
    .write();

  // Assign to Board

  Board.assign({
    columns: Columns,
    columnsOrder: ColumnsOrder
  }).write();

  res.send({
    columns: Columns,
    columnsOrder: ColumnsOrder
  });
});

router.put('/column/title', (req, res) => {
  const { idBoard, idColumn, title } = req.body;
  const Board = DB.get('boards').find({ id: idBoard });
  let BoardValue = Board.value();
  const Columns = Board.value().columns;

  Columns[idColumn].title = title;

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Изменено название колонки "${BoardValue.title}" в доске ${BoardValue.emoji}${
          BoardValue.title
        } на "${title}"`
      )
    )
    .write();

  Board.assign({ columns: Columns }).write();

  res.json(Columns);
});

router.delete('/column', (req, res) => {
  const { idBoard, idColumn } = req.body;
  let Board = DB.get('boards').find({ id: idBoard });
  let BoardValue = Board.value();
  let Columns = BoardValue.columns;
  let ColumnsOrder = BoardValue.columnsOrder;

  let indexColumn = ColumnsOrder.indexOf(String(idColumn));
  ColumnsOrder.splice(indexColumn, 1);

  delete Columns[String(idColumn)];

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Удалена колонка "${BoardValue.title}" в доске ${BoardValue.emoji}${BoardValue.title}`
      )
    )
    .write();

  Board.assign({ columns: Columns, columnsOrder: ColumnsOrder }).write();

  res.json({ columns: Columns, columnsOrder: ColumnsOrder });
});

module.exports = router;
