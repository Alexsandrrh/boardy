const router = require('express').Router();
const { DB, Schema } = require('../database');
const { history } = require('../database/history');

router.post('/card', (req, res) => {
  const { idBoard, idColumn, title } = req.body;
  const Board = DB.get(`boards`).find({ id: idBoard });
  const BoardValue = Board.value();
  let Columns = BoardValue.columns;
  let Cards = BoardValue.cards;

  let Card = Schema({
    title: title,
    idColumn: idColumn,
    idBoard: idBoard
  });

  Cards[String(Card.id)] = Card;
  Columns[String(idColumn)].cards.push(Card.id);

  Board.assign({
    columns: Columns,
    cards: Cards
  }).write();

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Создана новая карточка "${Card.title}" в доске ${BoardValue.emoji}${BoardValue.title}`
      )
    )
    .write();

  res.json({
    columns: Columns,
    cards: Cards
  });
});

router.post('/card/change', (req, res) => {
  const { idBoard, source, destination } = req.body;
  const Board = DB.get('boards').find({ id: idBoard });
  const BoardValue = Board.value();
  let Columns = BoardValue.columns;

  // Find column with CardMoved
  let lastColumn = Columns[String(source.droppableId)];
  // Find CardMoved and delete her from last column
  let cardMoved = lastColumn.cards.splice(source.index, 1)[0];
  // Add CardMoved to new column
  let newColumn = Columns[String(destination.droppableId)];
  newColumn.cards.splice(destination.index, 0, cardMoved);

  DB.get('histories')
    .push(
      history(
        { emoji: BoardValue.emoji, title: BoardValue.title, id: BoardValue.id },
        `Карточка с названием "${BoardValue.cards[cardMoved].title}" была перемещена из колонки "${
          lastColumn.title
        }" в колонку "${newColumn.title}"`
      )
    )
    .write();

  res.json(Columns);
});

module.exports = router;
